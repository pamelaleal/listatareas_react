import axios from 'axios';



export class ListService {
    
    baseUrl = "http://localhost:8080/api/list/";

     

    getAll(){
        return axios.get(this.baseUrl).then(res => res.data);
    }

    save(list) {
        return axios.post(this.baseUrl, list).then(res => res.data);
    }

     delete(id) {

        console.log(id);
        return axios.delete(`${this.baseUrl}${id}`).then(res => res.data);
        
    }
}